FROM centos:7
RUN yum update -y && yum install -y wget gcc openssl-devel bzip2-devel libffi-devel zlib-devel make
ADD https://www.python.org/ftp/python/3.7.12/Python-3.7.12.tgz /tmp
RUN cd /tmp && \
    tar xzf Python-3.7.12.tgz && \
    cd /tmp/Python-3.7.12 && \
    ./configure --enable-optimizations && \
    make altinstall
RUN python3.7 -m pip install flask flask-jsonpify flask-restful
RUN ["mkdir", "/python_api"]
COPY python-api.py /python_api
EXPOSE 5290
ENTRYPOINT ["python3.7", "/python_api/python-api.py"]
